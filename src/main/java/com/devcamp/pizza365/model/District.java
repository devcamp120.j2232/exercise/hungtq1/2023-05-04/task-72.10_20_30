package com.devcamp.pizza365.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "districts")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class District {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "name_district")
  private String nameDistrict;

  @Column(name = "prefix_district")
  private String prefixDistrict;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "region", nullable = false)
  private CRegion region;

  public District() {
  }

  public District(int id, String nameDistrict, String prefixDistrict) {
    this.id = id;
    this.nameDistrict = nameDistrict;
    this.prefixDistrict = prefixDistrict;
  }

  public District(int id, String nameDistrict, String prefixDistrict, CRegion region) {
    this.id = id;
    this.nameDistrict = nameDistrict;
    this.prefixDistrict = prefixDistrict;
    this.region = region;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNameDistrict() {
    return nameDistrict;
  }

  public void setNameDistrict(String nameDistrict) {
    this.nameDistrict = nameDistrict;
  }

  public String getPrefixDistrict() {
    return prefixDistrict;
  }

  public void setPrefixDistrict(String prefixDistrict) {
    this.prefixDistrict = prefixDistrict;
  }

  public CRegion getRegion() {
    return region;
  }

  public void setRegion(CRegion region) {
    this.region = region;
  }

}
