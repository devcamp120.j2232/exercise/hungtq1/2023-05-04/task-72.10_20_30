package com.devcamp.pizza365.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.model.District;
import com.devcamp.pizza365.repository.RegionRepository;

@Service
public class RegionService {
  @Autowired
  RegionRepository pRegionRepository;

  public ArrayList<CRegion> getAllRegion() {
    ArrayList<CRegion> regions = new ArrayList<>();
    pRegionRepository.findAll().forEach(regions::add);
    return regions;
  }

  public Set<District> getDistrictByRegionId(int id) {
    CRegion vRegion = pRegionRepository.findById(id);
    if (vRegion != null) {
      return vRegion.getDistricts();
    } else {
      return null;
    }
  }
}
